<a href="https://mnlee.gitlab.io/portfolio/#gitlab">Back to Margaret Lee's Portfolio</a>

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Execution](#execution)
* [Testing](#testing)
* [Output](#output)

## General info
Enabling Cross Origin Requests for a RESTful Web Service.  
	
## Technologies
Project is created with:
* Spring Boot version: 2.1.1
* Java version: 1.8
* Maven 3.3.9
	
## Setup
* Create project using https://start.spring.io/ or Spring Boot CLI with dependencies-- web

### Steps
* Create Greeting.java
* Create GreetingController.java

## Execution
* Ide > RunAs > Java Application OR
* mvn spring-boot:run OR
* create jar file
	- mvn clean install
	- java -jar <JARFILE>
	
## Testing
* Controller is working
	- http://localhost:8080/greeting
	- http://localhost:8080/greeting?name=Maggie
* cURL to test for success see #output for success
	 curl -v -X GET \
	-H "Origin: http://localhost:9000" \
	-H "Content-Type: application/json" \
	-d "{}" \
	http://localhost:8080/greeting?name=Max

* cURL to test for failure see #output for fail
	curl -v -X GET \
	-H "Origin: http://localhost:9001" \
	-H "Content-Type: application/json" \
	-d "{}" \
	http://localhost:8080/greeting?name=Max


	
## Output

### Output -- Success

> -H "Origin: http://localhost:9000" \
> -H "Content-Type: application/json" \
> -d "{}" \
> http://localhost:8080/greeting?name=Max
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /greeting?name=Max HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.47.0
> Accept: */*
> Origin: http://localhost:9000
> Content-Type: application/json
> Content-Length: 2
> 
* upload completely sent off: 2 out of 2 bytes
< HTTP/1.1 200 
< Vary: Origin
< Vary: Access-Control-Request-Method
< Vary: Access-Control-Request-Headers
< Access-Control-Allow-Origin: http://localhost:9000
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Mon, 24 Dec 2018 15:22:00 GMT
< 
* Connection #0 to host localhost left intact
{"id":1,"content":"Hello, Max!"}

### Output -- fail 
> -H "Origin: http://localhost:9001" \
> -H "Content-Type: application/json" \
> -d "{}" \
> http://localhost:8080/greeting?name=Max
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /greeting?name=Max HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.47.0
> Accept: */*
> Origin: http://localhost:9001
> Content-Type: application/json
> Content-Length: 2
> 
* upload completely sent off: 2 out of 2 bytes
< HTTP/1.1 403 
< Vary: Origin
< Vary: Access-Control-Request-Method
< Vary: Access-Control-Request-Headers
< Content-Length: 20
< Date: Mon, 24 Dec 2018 15:24:26 GMT
< 
* Connection #0 to host localhost left intact
Invalid CORS request




 