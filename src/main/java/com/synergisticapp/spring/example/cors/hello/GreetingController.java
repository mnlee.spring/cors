package com.synergisticapp.spring.example.cors.hello;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

/**
 * @RestController -- The Greeting object must be converted to JSON. No need to do this conversion manually because Jackson is on the classpath, 
 * Spring’s MappingJackson2HttpMessageConverter is automatically chosen to convert the Greeting instance to JSON.
 * @author mnlee
 *
 */
@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    /**
     * This @CrossOrigin annotation enables cross-origin requests only for this specific method. 
     * By default, its allows all origins, all headers, the HTTP methods. Specifying the value of one of 
     * the annotation attributes: origins, methods, allowedHeaders, exposedHeaders, allowCredentials or maxAge is possible.
     * it is also possible to add this annotation at controller class level as well, 
     * in order to enable CORS on all handler methods of this class.
     * See CorsApplication.java for global cors configuration
     * @param name
     * @return
     */
    @CrossOrigin(origins = "http://localhost:9000") 
    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(required=false, defaultValue="World") String name) {
        System.out.println("==== in greeting ====");
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

}